				

from abc import ABC

class Person(ABC):
	def getFullName(self):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self):
		pass


class Employee(Person):
	def __init__(self,firstName,lastName,email,department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def getFullName(self):
		return(f"{self._firstName} {self._lastName}")

	def addRequest(self):
		return "Request has been added"	

	def checkRequest(self):
		pass

	def addUser(self,firstName,lastName,email,department):
		pass

	def login(self):
		return(f"{self._email} has logged in")

	def logout(self):
		return(f"{self._email} has logged out")
	
	

class TeamLead(Person):
	def __init__(self,firstName,lastName,email,department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def checkRequest(self):
		pass

	def getFullName(self):
		return(f"{self._firstName} {self._lastName}")

	def addUser(self,firstName,lastName,email,department):
		pass

	def login(self):
		return(f"{self._email} has logged in!")

	def logout(self):
		return(f"{self._email} has logged out!")

	def addMember(self,Employee):
		return get_member.append(Employee)	

	def get_members(self):
		return get_member		

class Admin(Person):
	def __init__(self,firstName,lastName,email,department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def getFullName(self):
		return(f"{self._firstName} {self._lastName}")

	def checkRequest(self):
		pass

	def addUser(self,firstName,lastName,email,department):
		pass

	def login(self):
		return(f"{self._email} has logged in!")

	def logout(self):
		return(f"{self._email} has logged out!")

	def addUser(self):
		return("User has been added")										


class Request():
	def __init__(self,name,requester,dateRequested,status="Open"):
		self.name = name
		self.requester = requester
		self.dateRequested = dateRequested
		self.status = status

	def updateRequest(self):
		return "Request has been updated"

	def closeRequest(self):
		return "Request has been closed"

	def cancelRequest(self):
		return "Request has been canceled"
	def set_status(self,status):
		self.status = status	

get_member = []
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

employee1.getFullName()

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())